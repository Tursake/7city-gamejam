﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speedometer : MonoBehaviour {

	[SerializeField]
	public static float speed = 300;

	float animSpeed = 1;
	public static bool lightSpeed = false;
	float rotated = 0;

	[SerializeField]
	Animator mainChar;

	float speed_at_hit, slowed_speed;
	public static bool hit = false;
	bool hitRegistered = false;
	bool stunned = false;

	[SerializeField]
	RectTransform speedobar;

	[SerializeField]
	GameObject hitEffect;

	[SerializeField]
	AudioSource explosion;

	private IEnumerator coroutine;

	void Start(){
		speed = 300;
		lightSpeed = false;
		hit = false;
		hitEffect.SetActive(false);
		mainChar.speed = 0;
	}
	
	// Update is called once per frame
	void Update () {

		if(Countdown.started){
			if(hit && !hitRegistered){
				hitEffect.SetActive(true);
				explosion.Play();
				hitRegistered = true;
				speed -= 150;
				if(speed < 300){
					speed = 300;
				}
				Debug.Log("HIT");
				coroutine = hitStun();
				StartCoroutine(coroutine);
			}
			
			if(lightSpeed == false && !hit) speed += 0.7f;

			Vector3 barRot = speedobar.eulerAngles;

			if(rotated >= -165 && lightSpeed == false){
				barRot.z = -((speed-300)/10);
				rotated = -((speed-300)/10);
			} else if(lightSpeed == true){
				barRot.z = -((speed-300)*3);
			} else if(lightSpeed == false){
				lightSpeed = true;
			}

			speedobar.localRotation = Quaternion.Euler(barRot);

			if(!lightSpeed){
				mainChar.speed = 0.5f + ((speed - 300)/700);
			}

			if(Input.GetKeyDown(KeyCode.Space)){
				speed -= 200;
			}
		}
	}

	private IEnumerator hitStun(){

		yield return new WaitForSeconds(0.5f);
		hitEffect.SetActive(false);
		yield return new WaitForSeconds(0.5f);
		hit = false;
		hitRegistered = false;
	}
}
