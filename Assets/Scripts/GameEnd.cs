﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour {

	[SerializeField]
	RectTransform shop;
	
	[SerializeField]
	GameObject startText;
	
	[SerializeField]
	Animator mainChar;

	[SerializeField]
	GameObject win;
	[SerializeField]
	GameObject player;

	private IEnumerator coroutine;

	bool startMoving = false;
	bool ended = false;

	void Start(){
		win.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(Countdown.started){
			if(Speedometer.lightSpeed && !startMoving){
				coroutine = spawn();
				StartCoroutine(coroutine);
			}

			if(startMoving && shop.localPosition.x > 0){
				shop.localPosition = new Vector2(shop.localPosition.x -20, shop.localPosition.y);
			} else if (startMoving && shop.localPosition.x <= 0) {
				Speedometer.speed = 0;
				mainChar.speed = 0;
				startText.SetActive(true);
				startText.GetComponent<Text>().text = "Victory! You got the beer!\n-Press any key to restart-";
				win.SetActive(true);
				player.SetActive(false);
				ended = true;
			}

			if(ended){
				if(Input.anyKeyDown){
					SceneManager.LoadScene(0);
				}
			}
		}
	}

	private IEnumerator spawn(){
		yield return new WaitForSeconds(3f);
		startMoving = true;
	}
}
