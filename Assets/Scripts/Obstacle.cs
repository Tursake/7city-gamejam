﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
	
	void Start(){
		if(this.GetComponent<AudioSource>() != null){
			this.GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
		}
	}

	// Update is called once per frame
	void Update () {

		this.gameObject.GetComponent<RectTransform>().localPosition =
			new Vector2(this.gameObject.GetComponent<RectTransform>().localPosition.x - BG_Looper.movement,
						this.gameObject.GetComponent<RectTransform>().localPosition.y);

		if(this.gameObject.GetComponent<RectTransform>().localPosition.x < -1050 || BG_Looper.lightspeedMiddle){
			Destroy(this.gameObject);
		}

		if(this.gameObject.GetComponent<RectTransform>().localPosition.x <= -700 &&
			this.gameObject.GetComponent<RectTransform>().localPosition.x >= -800){

			if(this.gameObject.name == "Burd(Clone)"){
				if(!GameObject.Find("Char").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("Duck")){
					Speedometer.hit = true;
				}
			} else if(this.gameObject.name == "Cone(Clone)"){
				if(!GameObject.Find("Char").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("Jump")){
					Speedometer.hit = true;
				}
			}
		}
	}
}