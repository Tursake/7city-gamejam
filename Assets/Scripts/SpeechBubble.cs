﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeechBubble : MonoBehaviour {

	[SerializeField]
	GameObject bubble;
	[SerializeField]
	Text bubbleText;

	bool showingText = false;
	private IEnumerator coroutine;
	
	void Start(){
		bubble.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		if(!showingText && !Speedometer.lightSpeed && Countdown.started){
			showingText = true;
			coroutine = spawn();
			StartCoroutine(coroutine);
		}
	}

	private IEnumerator spawn(){

		bubble.SetActive(true);
		string[] textChoices = new string[]{"Kiire!", "Vauhtia!", "Kauppa\nsulkeutuu!", "Vittu!", "Ei saatana!", "Äkkiä!"};
		int rand = Random.Range(0,textChoices.Length);
		bubbleText.text = textChoices[rand];
		yield return new WaitForSeconds(1.5f);
		bubble.SetActive(false);
		int rand2 = Random.Range(1,3);
		yield return new WaitForSeconds(rand2);
		showingText = false;
		bubble.SetActive(false);
	}
}
