﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	[SerializeField]
	GameObject cone;
	[SerializeField]
	GameObject burd;

	bool spawning = false;

	private IEnumerator coroutine;
	
	// Update is called once per frame
	void Update () {
		if(!spawning && !Speedometer.lightSpeed && Countdown.started){
			spawning = true;
			coroutine = spawn(1.5f);
			StartCoroutine(coroutine);
		}
	}

	private IEnumerator spawn(float waitTime){

		GameObject temp;

		yield return new WaitForSeconds(waitTime);

		int rand = Random.Range(0,5);

		//Debug.Log(rand);

		if( rand <= 2){
			temp = Instantiate(cone, cone.GetComponent<RectTransform>());
			temp.gameObject.GetComponent<RectTransform>().SetParent(GameObject.Find("Obstacles").GetComponent<RectTransform>());
			temp.GetComponent<RectTransform>().localScale = cone.GetComponent<RectTransform>().localScale;
			temp.GetComponent<RectTransform>().localPosition = cone.GetComponent<RectTransform>().localPosition;
		} else if ( rand >= 3){
			temp = Instantiate(burd, burd.GetComponent<RectTransform>());
			temp.gameObject.GetComponent<RectTransform>().SetParent(GameObject.Find("Obstacles").GetComponent<RectTransform>());
			temp.GetComponent<RectTransform>().localScale = burd.GetComponent<RectTransform>().localScale;
			temp.GetComponent<RectTransform>().localPosition = burd.GetComponent<RectTransform>().localPosition;
		}
		
		spawning = false;
	}
}
