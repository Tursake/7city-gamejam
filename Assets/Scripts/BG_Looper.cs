﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BG_Looper : MonoBehaviour {

	[SerializeField]
	RectTransform backCity;
	[SerializeField]
	RectTransform backCity2;
	[SerializeField]
	RectTransform backCity3;
	[SerializeField]
	RectTransform bg1;
	[SerializeField]
	RectTransform bg2;
	[SerializeField]
	RectTransform bg3;
	public static float movement;
	[SerializeField]
	RectTransform lightSpeedImage;

	bool changedBG = false;

	[SerializeField]
	Image bg1_img;
	[SerializeField]
	Image bg2_img;
	[SerializeField]
	Image bg3_img;
	[SerializeField]
	Sprite black;

	[SerializeField]
	GameObject dashBoard;

	[SerializeField]
	AudioSource effect;
	bool audioPlayed = false;

	public static bool lightspeedMiddle = false;

	// Use this for initialization
	void Start () {
		lightspeedMiddle = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if(Countdown.started){
			movement = Mathf.Floor(((Speedometer.speed*Time.deltaTime) * 100)) / 100;

			moveFrontCity();
			moveBackCity();

			if(Speedometer.lightSpeed){
				lightSpeedImage.localPosition = new Vector2(lightSpeedImage.localPosition.x - movement*2,
															lightSpeedImage.localPosition.y);
				if(!audioPlayed){
					Countdown.stopMusic();
					effect.Play();
					audioPlayed = true;
				}
			}

			if(lightSpeedImage.localPosition.x <= 0 && changedBG == false){
				changeBG();
			}
		}
	}

	void moveBackCity(){

		backCity.localPosition = new Vector2(backCity.localPosition.x + movement/10, backCity.localPosition.y);
		backCity2.localPosition = new Vector2(backCity2.localPosition.x + movement/10, backCity2.localPosition.y);
		backCity3.localPosition = new Vector2(backCity3.localPosition.x + movement/10, backCity3.localPosition.y);

		if(backCity.localPosition.x > 1920){
			backCity.localPosition = new Vector2(backCity3.localPosition.x - 1920, backCity.localPosition.y);
		}
		if(backCity2.localPosition.x > 1920){
			backCity2.localPosition = new Vector2(backCity.localPosition.x - 1920, backCity2.localPosition.y);
		}
		if(backCity3.localPosition.x > 1920){
			backCity3.localPosition = new Vector2(backCity2.localPosition.x - 1920, backCity3.localPosition.y);
		}
	}

	void moveFrontCity(){
		
		bg1.localPosition = new Vector2(bg1.localPosition.x - movement, bg1.localPosition.y);
		bg2.localPosition = new Vector2(bg2.localPosition.x - movement, bg2.localPosition.y);
		bg3.localPosition = new Vector2(bg3.localPosition.x - movement, bg3.localPosition.y);

		if(bg1.localPosition.x < -1920){
			bg1.localPosition = new Vector2(bg3.localPosition.x + 1920, bg1.localPosition.y);
		}
		if(bg2.localPosition.x < -1920){
			bg2.localPosition = new Vector2(bg1.localPosition.x + 1920, bg2.localPosition.y);
		}
		if(bg3.localPosition.x < -1920){
			bg3.localPosition = new Vector2(bg2.localPosition.x + 1920, bg3.localPosition.y);
		}
	}

	void changeBG(){
		changedBG = true;
		bg1_img.sprite = black;
		bg2_img.sprite = black;
		bg3_img.sprite = black;
		dashBoard.SetActive(false);
		lightspeedMiddle = true;
	}
}
