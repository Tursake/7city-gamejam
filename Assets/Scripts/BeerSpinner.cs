﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerSpinner : MonoBehaviour {

	int spinSpeed = 1;

	void Start(){
		int randSpeed = Random.Range(0,5);

		if(randSpeed <= 2){
			spinSpeed = spinSpeed * -1;
		}
	}

	// Update is called once per frame
	void Update () {
		this.gameObject.GetComponent<RectTransform>().Rotate(new Vector3(0,0,spinSpeed));
	}
}
