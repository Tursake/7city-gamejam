﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerManager : MonoBehaviour {

	[SerializeField]
	GameObject[] beerCans;

	bool showingCans = false;

	// Use this for initialization
	void Start () {
		for(int i = 0; i < beerCans.Length; i++){
			beerCans[i].SetActive(false);
		}
	}

	void Update(){
		if(BG_Looper.lightspeedMiddle && !showingCans){
			showingCans = true;
			for(int i = 0; i < beerCans.Length; i++){
				beerCans[i].SetActive(true);
			}
		}
	}
	
}
