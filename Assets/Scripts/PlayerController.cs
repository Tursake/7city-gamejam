﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[SerializeField]
	Animator animator;

	[SerializeField]
	AudioSource playerEffect;
	[SerializeField]
	AudioClip jump;
	[SerializeField]
	AudioClip duck;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Countdown.started){
			if(Input.GetKeyDown(KeyCode.UpArrow)){
				playerEffect.clip = jump;
				playerEffect.Play();
				animator.SetTrigger("triggerJump");
			}

			if(Input.GetKeyDown(KeyCode.DownArrow)){
				playerEffect.clip = duck;
				playerEffect.Play();
				animator.SetTrigger("triggerDuck");
			}
		}
	}
}
