﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {

	[SerializeField]
	Text timerText;

	[SerializeField]
	RectTransform gameOver;

	[SerializeField]
	GameObject startText;

	[SerializeField]
	GameObject title;
	
	float timer = 59;
	//float timer = 10;
	float seconds = 0;

	public static bool started = false;

	private IEnumerator coroutine;

	[SerializeField]
	AudioSource music;
	[SerializeField]
	AudioClip intro;
	[SerializeField]
	AudioClip main;

	static bool stoppedMusic = false;

	bool starting = false;
	
	void Start(){
		started = false;
		music.clip = intro;
		stoppedMusic = false;
	}

	// Update is called once per frame
	void Update () {

		if(stoppedMusic){
			stoppedMusic = false;
			music.Stop();
		}

		if(Input.anyKeyDown && !started && !starting){
			starting = true;
			coroutine = start();
			StartCoroutine(coroutine);
			Speedometer.speed = 0;
		}

		if(started){
			seconds += Time.deltaTime;

			if(seconds % 60 >= 1 && !Speedometer.lightSpeed && started){
				timer -= 1;
				seconds = 0;
			}

			if(timer <= 0){
				timerText.text = "00:00";
				Speedometer.speed = 0;
				if(gameOver.localPosition.y > 0 ){
					gameOver.localPosition = new Vector2(gameOver.localPosition.x, gameOver.localPosition.y - 30);
				} else if(gameOver.localPosition.y <= 0){
					gameOver.localPosition = new Vector2(gameOver.localPosition.x, 0);
				}
			}
			else if(timer >= 10){
				timerText.text = "00:" + timer;
			} else if(timer < 10){
				timerText.text = "00:0" + timer;
			}
		}
	}

	private IEnumerator start(){
		yield return new WaitForSeconds(2);
		title.SetActive(false);
		startText.SetActive(false);
		started = true;
		Speedometer.speed = 300;
		music.clip = main;
		music.Play();
	}

	public static void stopMusic(){
		stoppedMusic = true;
	}
}
